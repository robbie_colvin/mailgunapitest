﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MailgunTest
{
  class Program
  {
    static RestClient client = new RestClient();
    static string[] initArgs;
    static string domain = ConfigurationManager.AppSettings["domain"];

    static void Main(string[] args)
    {
      initArgs = args;
      client.BaseUrl = new Uri("https://api.mailgun.net/v3");
      client.Authenticator = new HttpBasicAuthenticator("api", ConfigurationManager.AppSettings["apiKey"]);

      bool valid = false;
      while (!valid)
      {
        Console.Out.WriteLine(@"Please make a selection:
1) Send To Email
2) Send To Mailing List
3) Create New Mailing List
4) Add Member To List
5) Get Mailing List Members
6) Remove Member
7) List Unsubscribes
8) List Mailing Lists
9) List Bounces
10) Check if Email Has Bounced
11) List Failed
12) Exit");
        string selection = Console.In.ReadLine();

        switch (selection)
        {
          case "1":
            Console.Clear();
            SendToEmail("email");
            break;
          case "2":
            Console.Clear();
            SendToEmail("list");
            break;
          case "3":
            Console.Clear();
            AddMailingList();
            break;
          case "4":
            Console.Clear();
            AddMemberToList();
            break;
          case "5":
            Console.Clear();
            GetMailingListMembers();
            break;
          case "6":
            Console.Clear();
            RemoveMemberFromList();
            break;
          case "7":
            Console.Clear();
            ViewUnsubscribes();
            break;
          case "8":
            Console.Clear();
            ListMailingLists();
            break;
          case "9":
            Console.Clear();
            ListBounces();
            break;
          case "10":
            Console.Clear();
            HasEmailBounced();
            break;
          case "11":
            Console.Clear();
            ListFailed();
            break;
          case "12":
            Environment.Exit(0);
            break;
          default:
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Out.WriteLine("Not a valid option...\n");
            Console.ResetColor();
            valid = false;
            break;
        }
      }
    }

    public static void ViewUnsubscribes()
    {
      RestResponse response = (RestResponse)ViewUnsubscribes("");
      JsonObject mailGunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.Out.WriteLine(mailGunResponse["items"] + "\n");

      Main(initArgs);
    }
    public static IRestResponse ViewUnsubscribes(string list)
    {
      RestRequest request = new RestRequest();
      request.AddParameter("domain", domain, ParameterType.UrlSegment);
      request.Resource = "{domain}/unsubscribes";
      return client.Execute(request);
    }

    public static void RemoveMemberFromList()
    {
      Console.Out.WriteLine("\nEnter Mailing List Email Address Alias: ");
      string listAlias = Console.In.ReadLine();

      Console.Out.WriteLine("\nEnter Email Of Member To Remove: ");
      string memberToRemove = Console.In.ReadLine();

      RestResponse response = (RestResponse)RemoveMemberFromList(listAlias, memberToRemove);
      JsonObject mailGunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.Clear();

      Console.ForegroundColor = ConsoleColor.Green;
      Console.Out.WriteLine("\n" + mailGunResponse["message"] + "\n");
      Console.ResetColor();

      Main(initArgs);
    }

    public static IRestResponse RemoveMemberFromList(string listAlias, string memberToRemove)
    {
      RestRequest request = new RestRequest();
      request.Resource = "lists/{list}/members/{member}";
      request.AddParameter("list", listAlias, ParameterType.UrlSegment);
      request.AddParameter("member", memberToRemove, ParameterType.UrlSegment);
      request.Method = Method.DELETE;
      return client.Execute(request);
    }

    public static void GetMailingListMembers()
    {
      Console.Out.WriteLine("Enter Mailing List Email Address Alias");
      string listAlias = Console.In.ReadLine();

      RestResponse response = (RestResponse)GetMailingListMembers(listAlias);
      if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
      {
        Console.Out.WriteLine("\n:( Mailing List not found or incorrect format.\n");
        Main(initArgs);
      }

      JsonObject mailGunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.ForegroundColor = ConsoleColor.Blue;
      Console.Out.WriteLine("\nTotal Members On List: " + mailGunResponse["total_count"] + "\n");
      Console.ForegroundColor = ConsoleColor.Green;
      JsonArray members = (JsonArray)SimpleJson.DeserializeObject(mailGunResponse["items"].ToString());

      int i = 0;
      foreach (var member in members)
      {
        i++;
        JsonObject memberObj = (JsonObject)SimpleJson.DeserializeObject(member.ToString());
        Console.Out.WriteLine("Name: " + memberObj["name"] + "\nEmail: " + memberObj["address"] + "\nSubscribed: " + memberObj["subscribed"] + "\n");
      }
      Console.ResetColor();

      //Console.In.Read();
      Main(initArgs);
    }

    public static IRestResponse GetMailingListMembers(string listAlias)
    {
      RestRequest request = new RestRequest();
      request.Resource = "lists/{list}/members";
      request.AddParameter("list", listAlias, ParameterType.UrlSegment);
      return client.Execute(request);
    }

    public static void AddMailingList()
    {
      Console.Out.WriteLine("\nEnter New List Name:");
      string name = Console.In.ReadLine();

      Console.Out.WriteLine("\nEnter New List Alias Address:");
      string address = Console.In.ReadLine();

      Console.Out.WriteLine("\nEnter New List Description:");
      string description = Console.In.ReadLine();

      RestResponse response = (RestResponse)AddMailingList(name, address, description);
      JsonObject mailgunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.Clear();
      Console.ForegroundColor = ConsoleColor.Green;
      Console.Out.WriteLine("\n" + mailgunResponse["message"] + "\n");
      Console.ResetColor();
      Main(initArgs);
    }

    public static void ListMailingLists()
    {
      RestRequest request = new RestRequest();
      request.Resource = "/lists";
      RestResponse response = (RestResponse)client.Execute(request);
      JsonObject mailgunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);
      Console.ForegroundColor = ConsoleColor.Blue;
      Console.Out.WriteLine("\nCurrent Available Lists:\n");
      Console.ForegroundColor = ConsoleColor.Green;
      JsonArray things = (JsonArray)mailgunResponse["items"];
      foreach (JsonObject thing in things)
      {
        Console.Out.WriteLine(thing["address"]);
      }
      Console.Out.WriteLine("\n");
      Console.ResetColor();
    }

    public static IRestResponse AddMailingList(string newListName, string newListAddress, string newListDescription)
    {
      RestRequest request = new RestRequest();
      request.Resource = "lists";
      request.AddParameter("name", newListName);
      request.AddParameter("address", newListAddress);
      request.AddParameter("description", newListDescription);
      request.Method = Method.POST;
      return client.Execute(request);
    }

    public static void AddMemberToList()
    {
      Console.Out.WriteLine("\nEnter List Address:");
      string listAddress = Console.In.ReadLine();

      Console.Out.WriteLine("\nEnter Member Name: ");
      string memberName = Console.In.ReadLine();

      Console.Out.WriteLine("\nEnter Member Email: ");
      string memberEmail = Console.In.ReadLine();

      MailAddress memberToAdd = new MailAddress(memberEmail, memberName);

      RestResponse response = (RestResponse)AddMemberToList(listAddress, memberToAdd);
      JsonObject mailgunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.Clear();
      Console.ForegroundColor = ConsoleColor.Green;
      Console.Out.WriteLine("\n" + mailgunResponse["message"] + "\n");
      Console.ResetColor();

      Main(initArgs);
    }

    public static IRestResponse AddMemberToList(string list, MailAddress memberToAdd)
    {
      RestRequest request = new RestRequest();
      request.Resource = "lists/{list}/members";
      request.AddParameter("list", list, ParameterType.UrlSegment);
      request.AddParameter("address", memberToAdd.Address);
      request.AddParameter("subscribed", true);
      request.AddParameter("name", memberToAdd.DisplayName);
      request.AddParameter("description", "Member");
      request.Method = Method.POST;

      return client.Execute(request);
    }

    public static void ListFailed()
    {
      RestResponse response = (RestResponse)ListFailedExecute();
      JsonObject mailgunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.Clear();
      Console.ForegroundColor = ConsoleColor.Green;
      Console.WriteLine("\n" + mailgunResponse["stats"] + "\n");
      Console.ResetColor();
      Main(initArgs);
    }

    public static IRestResponse ListFailedExecute()
    {
      RestRequest request = new RestRequest();
      request.AddParameter("domain", domain, ParameterType.UrlSegment);
      request.Resource = "{domain}/stats/total";
      request.AddParameter("event", "failed");
      request.AddParameter("duration", "1m");

      return client.Execute(request);
    }

    public static void ListBounces()
    {
      RestResponse response = (RestResponse)ListBounces("");
      JsonObject mailgunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.Clear();
      Console.ForegroundColor = ConsoleColor.Green;
      Console.WriteLine("\n" + mailgunResponse["items"] + "\n");
      Console.ResetColor();
      Main(initArgs);
    }

    public static void HasEmailBounced()
    {
      Console.WriteLine("\nEnter Email To Check");
      string emailToCheck = Console.ReadLine();
      RestResponse response = (RestResponse)ListBounces(emailToCheck);
      JsonObject mailgunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.Clear();
      Console.ForegroundColor = ConsoleColor.Green;
      JsonArray responseArray = (JsonArray)mailgunResponse[0];

      foreach(var thing in responseArray)
      {
        Console.WriteLine(thing.ToString());
      }
      Console.ResetColor();
      Main(initArgs);
    }

    public static IRestResponse ListBounces(string email)
    {
      RestRequest request = new RestRequest();
      if (email != "") { request.AddParameter("address", email); }
      request.AddParameter("domain", domain, ParameterType.UrlSegment);
      request.Resource = "{domain}/bounces";
      request.Method = Method.GET;

      return client.Execute(request);
    }

    public static void SendToEmail(string sendType)
    {
      Console.Out.WriteLine("\nEnter Name" + (sendType == "list" ? " Of List" : "") + ": ");
      string name = Console.In.ReadLine();

      Console.Out.WriteLine("\nEnter " + sendType + " address: ");
      string address = Console.In.ReadLine();
      MailAddress toAddress = new MailAddress(address, name);

      Console.Out.WriteLine("\nEnter Subject: ");
      string subject = Console.In.ReadLine();

      Console.Out.WriteLine("\nEnter Body Text: ");
      string body = Console.In.ReadLine();

      Console.Out.WriteLine("\nEnter HTML Version");
      string htmlbody = Console.In.ReadLine();

      RestResponse response = (RestResponse)SendMessage(toAddress, subject, body, htmlbody);
      JsonObject mailGunResponse = (JsonObject)SimpleJson.DeserializeObject(response.Content);

      Console.Clear();
      Console.ForegroundColor = ConsoleColor.Green;
      Console.Out.WriteLine("\nMessage with ID: " + mailGunResponse["id"] + " has been queued! " + mailGunResponse["message"] + "\n");
      Console.ResetColor();

      Main(initArgs);
    }

    public static IRestResponse SendMessage(MailAddress toAddress, string subject, string body, string htmlbody)
    {
      RestRequest request = new RestRequest();
      request.AddParameter("domain", domain, ParameterType.UrlSegment);
      request.Resource = "{domain}/messages";
      request.AddParameter("from", ConfigurationManager.AppSettings["fromAddress"]);
      request.AddParameter("to", toAddress.Address);
      request.AddParameter("h:Reply-To", ConfigurationManager.AppSettings["replyAddress"] != "" ? ConfigurationManager.AppSettings["replyAddress"] : "");
      request.AddParameter("subject", subject);
      if (body != "") { request.AddParameter("text", "To view the HTML version of this email, please go here:.. " + body); }
      if (htmlbody != "") { request.AddParameter("html", htmlbody); }

      // Adding file attachments: pretty easy
      //request.AddFile("attachment", Path.Combine("files", "gm-flat.png"));

      // Adding inline images is also easy
      //request.AddFile("inline", "path/filename.jpg");
      // Add in html as: <img src=\"cid:filename.jpg\"></html>");

      // Attach HTML Email from File
      //string[] htmlBodyFromFile = File.ReadAllLines(Path.Combine("files", "ThymeHolidays2015-inline.html"));
      //string htmlBodyMain = "";
      //foreach(string line in htmlBodyFromFile){
      //    htmlBodyMain += line + "\n";
      //}
      //request.AddParameter("html", htmlBodyMain);

      request.Method = Method.POST;
      return client.Execute(request);
    }

  }
}
