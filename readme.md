MailGun API Test
================

To build, rename app.config.outline to just app.config in the MailgunTest folder and edit the sections:

```
<appSettings>
	<add key="apiKey" value="key-0000000000000000000000000000000000"/>
	<add key="domain" value="DOMAIN.com"/>
	<add key="fromAddress" value="NAME &lt;EMAIL@DOMAIN.com&gt;"/>
	<add key="replyAddress" value="REPLY-TO-NAME %lt;REPLY-TO-EMAIL-ADDRESS&gt;"/>
</appSettings>
```